import { Component } from '@angular/core';

@Component({
  selector: 'flagship-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'flagship';
}
