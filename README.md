Contributions

<!-- ALL-CONTRIBUTORS-BADGE:START - Do not remove or modify this section -->

[![All Contributors](https://img.shields.io/badge/all_contributors-0-orange.svg?style=flat-square)](#contributors-)
[![codecov](https://codecov.io/gh/brandonmillerio/Flagship/branch/main/graph/badge.svg?token=4MOGSVG6DI)](https://codecov.io/gh/brandonmillerio/Flagship)
[![CodeFactor](https://www.codefactor.io/repository/github/brandonmillerio/flagship/badge)](https://www.codefactor.io/repository/github/brandonmillerio/flagship)

<!-- ALL-CONTRIBUTORS-BADGE:END -->

Contributions to our Flagship modules are highly appreciated!

## Flagship Contribution Process

Flagship uses Git Flow to help keep a regular changelog. Below is an example of git flow:

![Flagship Contribution Process](https://buddy.works/blog/images/gitflow.png)

Here's an great video that explains the [Flagship Dev Process](https://www.youtube.com/watch?v=LxJpwYzoO9A).

### If you would like to contribute, but are having problems understanding the process, help can be provided!

### Step 1: Fork the repository

To fork an repository, click on the Button **Fork** at the top right of the repository. See below:

![Fork Button](https://user-images.githubusercontent.com/8295517/131223533-cb85a7a8-c97d-4427-b502-88b5bda1daf7.png)

### Step 2: Clone the repository

#### Pleasee make sure you clone from YOUR fork of the repo.

```shell
git clone git@github.com:YOUR_USERNAME/REPOSITORY_NAME.git
# Clones your fork of the repository into the current directory in terminal on your computer
```

### Step 3: Initialize git-flow

```shell
git flow init
# Initializes git-flow for your repository
```

Please make sure that the branch name for production releases is `master` and the branch name for development is `develop`!

### Step 4: Make changes and push code

If you would like to fix issues, please use issue number as feature branch name.

```shell
git flow feature start <name of your feature>
# code your changes
git add .
git commit -m""
# repeat the last three lines until you finished
git flow feature finish <name of your feature>
git push origin develop
```

### Step 5: Create a pull request on Github

This [article](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html) describes in a very good way how to create a pull request on the Github platform.

Please make sure that you ALWAYS create pull request for the `develop` branch. Pull requests to the `master` branch will be closed.

The only exception are `hotfix` branches which are fixing critical bugs on the existing release.

### Step 6: Review the pull request (Only for Flagship Team)

The lead developers of the module have to review the pull request by going through the single commits. If the pull request is acceptable, they merge the pull request in the code base of the Flagship module.

Additional acceptance criteria:

- Please make sure that the pull request branch passes the build. We will get to it as soon as possible.
- The Flagship module code base should pass the .husky pre-commit check. Coding Standard violations have to be fixed by the developer of the pull request or the person who merges the pull request.

### Step 7: Show :heart: to contributors

We added [all-contributors-bot](https://allcontributors.org/) to our repositories, so you can use this bot to add contributors to our README.de

- Basic usage: Add comment to issue or PR: `@all-contributors please add @Someone for code, doc and infra`
- you can choose from [different contribution types](https://allcontributors.org/docs/en/emoji-key)
- The bot creates PR with updated README.md and .all-contributorsrc file

## Contributors ✨

Thanks goes to these wonderful people ([emoji key](https://allcontributors.org/docs/en/emoji-key)):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<!-- markdownlint-restore -->
<!-- prettier-ignore-end -->

<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [all-contributors](https://github.com/all-contributors/all-contributors) specification. Contributions of any kind welcome!
