# Security Policy

## Supported Versions

Use this section to tell people about which versions of your project are
currently being supported with security updates.

| Version | Supported          |
| ------- | ------------------ |
| 0.0.1   | :white_check_mark: |

## Reporting a Vulnerability

If you find any vulnerabilities at all please submit a ticket with the label VULNERABILITY and it will go directly to the top of the list.
Thank you for allowing us to remain secure. If you happen to find a vulnerability and we confirm/fix it, your name will be added to this document (if you want), as a special tank you!


Flagship SecOps Ninjas:

 - Your github account here!
